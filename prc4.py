import busio 

import digitalio 

import board 

import adafruit_mcp3xxx.mcp3008 as MCP 

from  adafruit_mcp3xxx.analog_in import AnalogIn 

import RPi.GPIO as GPIO 

import time 

import threading 

  

btn_submit = 24 #GPIO PIN of the push button 

sampling_rates = [10,5,1] #array of possible sampling rate options 

sampling_index = 0 #initial sampling rate is 10 

sampling_rate = 10 #initial sampling rate is set to 10 

ON = True #current state of program 

time_start = time.time() #starting time 

  

def inc_sampling_rate(channel): 

    global sampling_rates, sampling_index, sampling_rate  

    sampling_index = sampling_index+1 #button press increases the sampling rate to the next in 	the array 

    if (sampling_index == 3): #if sampling rate was 10, loops back to beginning of array, 		otherwise moves to next sampling rate in array  

  	    sampling_index = 0 

    sampling_rate = sampling_rates[sampling_index]  

    print(f"<<SAMPLING RATE SET TO {sampling_rate}s>>") #shows user the current sampling 	rate 

  

  

#specify GPIO mode and setup button 

GPIO.setup(btn_submit, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #BUTTON 

GPIO.add_event_detect(btn_submit, GPIO.FALLING, callback=inc_sampling_rate, bouncetime=200) #DEBOUNCE + callback +interrupt 

  

#create the spi bus 

spi = busio.SPI(clock = board.SCK, MISO = board.MISO, MOSI = board.MOSI) 

  

#create the cs 

cs = digitalio.DigitalInOut(board.D5) 

  

#create the mcp pbject 

mcp = MCP.MCP3008(spi, cs) 

  

#create an analog input channel on pin 0 and pin 1 

chan = AnalogIn(mcp, MCP.P0) 

chan1 = AnalogIn(mcp, MCP.P1) 

 

def data_read(): 

    global ON 

    global sampling_rate 

    global time_start 

  

    strings = ["Runtime", "Temp Reading", "Temp", "Light Reading"]#array of strings for the 		purpose of making formatting header easy 

    output = "" 

  

    for word in strings:#prints left aligned table 

  		a_string = word 

  		aligned_string = "{:<25}".format(a_string) 

  		output += aligned_string 

    print(output+"\n") 

  

    while ON: #code runs while state of program is ON 

  		time_data = time.time()-time_start #current time with starting time treated as zero 

  		time_data = int(time_data) #rounds to nearest int 

  		time_data = str(time_data)+"s" 

  		temp = chan1.voltage #obtain temperature data from the sensor  

  		temperature  = (temp-0.5)*100  

  		temperature = str(temperature)+" C"  

  		light = chan.value  

  		data = [time_data, chan1.value, temperature, light] #data to be entered as row in 		the table 

  		output = "" 

  

  		for word in data: #prints data in same format as header 

   		a_string = word 

   		aligned_string = "{:<25}".format(a_string) 

   		output += aligned_string 

  

  		print(output+"\n") 

  		time.sleep(sampling_rate)#samples data at sampling rate defined by sampling_rate 

  

  

  

  

print("WELCOME TO DATA READ, SELECT SAMPLING FREQUENCY USING THE BUTTON AND PRESS ENTER TO EXIT") 

x = threading.Thread(target=data_read) #data reading runs in its own thread 

x.start() #starts thread to read data 

  

#allows user to exit program by pressing Enter  

while ON: 

    end_prog = input() 

    if (end_prog == ""): 

        ON = False 

 

 

 
